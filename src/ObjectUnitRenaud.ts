/**
 * Object unit
 */

type NETWORK = "facebook" | "linkedin" | "twitter";

/*
 *  Create class Contact with properties
 *  1 - use export keyword
 *  2 - use class keyword
 *  3 - name this class Contact
 *  4 - In parameter of constructor add public properties:
 *      email (string)
 *      phone with Array of properties :
 *          country: number,
 *          number: number
 *      network: add properties in function of contact (optional or another) must take string for ids' record.
 */
export class AddressBookContact {

    constructor(
        public email: string,
        public phone: Array<{
            country: number,
            number: number
        }>,
        public network: Record<NETWORK, string|undefined>
    ) {}
}

/*
 public network: Record<NETWORK, string|undefined>
 network: {
    "linkedin": "renaud.racinet",
    "facebook": undefined,
    "twitter": undefined
 }
 */

/*
*  public network: {
*     linkedin?: string
*     facebook?: string
*     twitter?: string
*  }
* */

interface DateRange {
    delivery: Date,
    expire: Date
}

class Address {
    constructor(
        public street: string,
        public number: number,
        public zip: number,
        public city: string,
        public country: string
    ) {}
}

/*
 *  Create below an object with class form
 *  1 - use export keyword
 *  2 - use class keyword
 *  3 - name this class AddressBook
 *  4 - In parameter of constructor add public properties:
 *      id (number),
 *      names with properties:
 *          first (string)
 *          last (string)
 *      date: add properties in function (optional or another)
 *            e.g.: birthday (Date),
 *                  carLicense: { delivery: Date, expire: Date },
 *                  idCard: { deliver: Date, expire: Date }
 *      address: { street: string, number: number, zip: number, city: string, country: string } or must be an array another Object must be a class same properties
 *      contact takes Contact Class
 *      register must be a date
 *      isActive must be a boolean
 */
export class AddressBook {

    constructor(
        public id: number,
        public names: {
            first: string,
            last: string,
        },
        public dates: {
            birthday: Date,
            carLicense: DateRange,
            idCard: DateRange,
        },
        public address: Address,
        public contact: AddressBookContact,
        public register: Date,
        public isActive: boolean
    ) {}
}

/**
 *  Object Unit class for testing
 */
export default class ObjectUnit {

    /*
     *  add type Array of AddressBook
     */
    public contacts: any;

    constructor() {
        /*
         *  Assign contacts an AddressBook object with id 1, names: { first: "Saleck", last: "El Jili" } and oother informations
         */
    }

    /*
     *  Create method it must return firstname following by lastname in function of id in parameter. In capitalize for each.
     */
    public getNames = (id: number ): string => {
        const CONTACT = this.contacts.find( (x: any) => x.id === id);
        const FIRSTNAME = CONTACT?.names?.first?.split(" ")
            .map( (word: string) => word?.charAt(0).toUpperCase() + word?.slice(1).toLowerCase() )
            .join(" ");
        const LASTNAME = CONTACT?.names?.last?.split(" ")
            .map( (word: string) => word?.charAt(0).toUpperCase() + word?.slice(1).toLowerCase() )
            .join(" ");
        return `${FIRSTNAME} ${LASTNAME}`;
    };

    /*
     * Create method it must return initial of firstname and lastName in capitalize.
     */
    public getSimplifyNames = (id: number) : any => {
        const CONTACT = this.contacts.find( (x: any) => x.id === id);
        const FIRSTNAME = CONTACT?.names?.first?.charAt(0).toUpperCase() + ".";
        const LASTNAME = CONTACT?.names?.last?.split(" ")
            .map( (word: string) => word?.charAt(0).toUpperCase() + word?.slice(1).toLowerCase() )
            .join(" ");
        return `${FIRSTNAME} ${LASTNAME}`;
    }

    /*
     *  Create method it must return birthday date in function of id in parameter.
     */
    public getBirthday = (id: number): string => {
        const CONTACT = this.contacts.find( (x: any) => x.id === id);
        return CONTACT?.dates?.birthday?.toISOString();
    }

    /*
     *  Create method, it must return Birthday's year of id in parameter.
     */
    public getYearBirthday = (id: number): string => {
        const CONTACT = this.contacts.find( (x: any) => x.id === id);
        return CONTACT?.dates?.birthday?.toISOString().split("T")[0].split("-")[0];
    }

    /*
     *  Create method, it must return actual age in function of id in parameter.
     */
    public getAge = (id: number): number => {
        return Number(new Date()?.getFullYear()) - Number(this.getYearBirthday(id));
    }

    /*
     *  Create method, it must return full address in function id.
     */
    public getAddress = (id: number): string => {
        const CONTACT = this.contacts?.find( (x: any) => x?.id === id);
        const { number, street, zip, city } = CONTACT?.address;
        return `${number}, ${street}\n${zip} ${city}`;
    }

    /*
     *  Create method, it must return network profile in function of id and one platform network (Facebook).
     */
    public getNetwork = (id: number, socialNetwork: string ): string => {
        const CONTACT = this.contacts?.find( (x: any) => x?.id === id);

        switch (socialNetwork) {
            case 'facebook':
                if (CONTACT?.contact?.network[socialNetwork]) {
                    return "https://www.facebook.com/" + CONTACT?.contact?.network[socialNetwork];
                }
                return 'No data for ' + socialNetwork;
            case 'linkedin':
                if (CONTACT?.contact?.network[socialNetwork]) {
                    return 'https://www.linkedin.com/in/' + CONTACT?.contact?.network[socialNetwork];
                }
                return 'No data for ' + socialNetwork;
            default:
                return 'Nothing found for social network: ' + socialNetwork;
        }
    }

    /*
     *  Create method, it must return all contacts which have active.
     */
    public getAllContactsIsActive = (): Array<any> => {
        return this.contacts?.filter( (x: any) => x?.isActive === true);
    }

    /*
     *  Create method, it must return all contacts born in 1998 and registering in 2020 for example.
     */
    public getAllContactsBornAndRegister = (borned: number, registered: number): any => {
        return this.contacts?.filter( (x: any) => {
            return Number(this.getYearBirthday(x?.id)) === borned &&
                Number(x?.register?.toISOString()?.split("T")[0]?.split("-")[0]) === registered
        })
    }

    /*
     *  Create method with all contacts with email which domain name is orange
     */
    public getAllContactsWithEmailProvider = (provider: string): any => {
        return this.contacts.filter( (x: any) => x?.contact?.email.match(`(?<=@)${provider}(?=.[a-z]{2})`));
    }
}

