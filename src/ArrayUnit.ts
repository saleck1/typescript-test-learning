export class ArrayUnit {

    constructor() {}

    /*
     *  Create a method for incremental each number of array
     */
    public mapArrayIncremental = (ar: Array<number>,n: number): Array<number> =>{
        return ar.map(x =>x+n)
    }

    /*
     *  Create a method for incremental each number of array
     */
    public mapArrayDecremental = (ar: Array<number>,n: number): Array<number> =>{
        return ar.map(x =>x-n)
    }

    /*
     *  Create a method for add two numbers
     */
    public operationAddition = (a:number,b: number): number =>{
        return a+b
    }

    /*
     *  Create a method for subtract two numbers
     */
    public operationSubtraction = (a: number,b: number): number =>{
        return a-b
    }

    /*
     *  Create a method for multiply two numbers
     */
    public operationMultiply = (a: number,b: number): number =>{
        return a*b
    }

    /*
     *  Create a method for divide two numbers
     */
    public operationDivide = (a: number,b: number): number =>{
        return a/b
    }

    /*
     *  Create a method for pass each operation previous created for each number of array
     */
    public mapArrayOperation = (ar: Array<number>, operand: number, operation: (a: number, b: number) => number): Array<number> => {
        return ar.map( (x: number) => operation(x, operand) );
    }

    /*
     *  Create a method for will find sum of array of number
     */
    public sum = (ar: Array<number>): number => {
        // return ar.reduce( (acc: number, iter: number) => acc + iter);
        let num: number = 0
        ar.map( (x: number) => num = num + x)
        return num
    }

    /*
     *  Create a method for flatten a two dimension array.
     */
    public flatMap = (ar: Array<Array<number>>) : Array<number> =>{
        // return arr.reduce( (acc: Array<number>, iter: Array<number) => acc.concat(iter) );
        return  ar[0].concat(ar[1])
    }

    /*
     * Create a method to create an array with range with a start and en end.
     */
    public range = (first:number, last: number) : Array<number> =>{
        /* Array.from(
        Array(last))
            .keys()
            ).map( (x: number) => x + start);
         */

        let arr: Array<number> = [];
        for(let i=0; i <= last - first ; i++)
            arr[i] = first + i
        return arr
        /*let arr= new Array<number>()
        return arr.map((x,a)=>(x+a+1))*/
    }

    /*
     *  Create a method for sort numbers in array .
     */
    public sortAscend = (arr: Array<number>): Array<number> =>{
        return arr.sort((a: number, b: number) => this.operationSubtraction(a, b));
    }

    /*
     *  Create a method for unsort numbers in array .
     */
    public sortDescend = (arr: Array<number>): Array<number> =>{
        return arr.sort((a:number, b:number) => this.operationSubtraction(b, a) );
    }

    /*
     *  Create a method for sort a two dimensions array unsorted
     */
    public acceptanceTestingForNumbers = (arr: Array<Array<number>>): Array<Array<number>> =>{
        /*
            this.sortAscend(this.flatMap(arr));
         */

        arr.sort((a, b) => a[0]-b[0]);
        return arr.sort((a, b) => a[1]-b[1]);
    }

    /*
     *  Create a methods for sort by country in alphabetic order
     */
    public sortStringToLocale = (arr: Array<string>, st: string)=>{
        /*
         *  arr.sort( (a: string, b: string) => a.localeCompare(b, st);
         */

        if(st=='en') return arr.sort()
        return arr.sort(function (a:string, b:string) {
            return a.localeCompare(b);
        });
    }
}
