import * as express from "express";
import {Application} from "express";
import * as cors from "cors";
import * as bodyParser from "body-parser";

import routes from "./routes/Route";
import routes2 from "./routes/PostRoute";

class App {

    public application: Application = express();

    constructor() {
        this.configuration();
        this.routerConfiguration();
        this.initServer();
    }

    /* Do init config with cors and body-parser modules, do not hesitate to read documentations */
    private configuration(): void {
        this.application.use(cors())
        this.application.use(bodyParser.json())
        this.application.use(bodyParser.urlencoded({ extended: false }))
    }

    /* create router configuration */
    private routerConfiguration(): void {
        /* create folder routes and controllers before */
        this.application.use('/api/v1/', routes);
        
        this.application.use('/api/v1/', routes2);
    }

    /* Init the server with http server listen */
    private initServer(): void {
        const PORT: number = 8060
        this.application.listen(PORT, () => {
            return console.log("server listening on port %d", PORT);
        });
    }
}

export default new App().application;
