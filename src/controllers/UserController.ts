import axios, {AxiosError, AxiosResponse} from "axios";
import Model from "../Models/Model";

export default class UserController {

    constructor() {}

    public getAllUser = (): Promise<any> => {
        return axios.get("https://jsonplaceholder.typicode.com/users")
                .then((response: AxiosResponse<Model>) => response.data)
                .catch((err: AxiosError) => {
                    throw new Error(`${JSON.stringify(err.toJSON())}`);
                })
                .finally(() => console.log("It's finish"));
    }


    public getOneUser = (userId: string): Promise<any> => {
        return axios.get("https://jsonplaceholder.typicode.com/users/" + userId)
            .then((response: AxiosResponse<Model>) => response.data)
            .catch((err: AxiosError) => {
                throw new Error(`${JSON.stringify(err.toJSON())}`);
            })
            .finally(() => console.log("It's done."));
    }






    /*public postOneUser = (payload: Model): any => {
        return axios.post("https://jsonplaceholder.typicode.com/users", payload)
                .then((response: AxiosResponse<Model>) => response.data)
                .catch((err: AxiosError) => {
                    throw new Error(`${JSON.stringify(err.toJSON())}`);
                })
                .finally(() => console.log("It's done."));
    }*/



}

