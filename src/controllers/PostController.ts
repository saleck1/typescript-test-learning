import axios from "axios";

export default class PostController {
    constructor() {
    }

    public getAllPosts(): Promise<any>{
        return axios.get('https://jsonplaceholder.typicode.com/posts/')
            .then(response => response.data)
            .catch(er=>{
                throw new Error(`${JSON.stringify(er.toJSON())}`);
            })
            .finally(()=>'post is finich')
    }


    public getOnePost(id:string): Promise<any>{
        console.log('https://jsonplaceholder.typicode.com/posts/' + id);
        return axios.get('https://jsonplaceholder.typicode.com/posts/' + id)
            .then(response => response.data)
            .catch(er=>{
                throw new Error(`${JSON.stringify(er.toJSON())}`);
            })
            .finally(()=>'post by id is finich')
    }
}
