/**
 *  Class for training to typescript
 *
 *  For begin. see "https://www.typescriptlang.org/docs/handbook/typescript-from-scratch.html"
 *  For learn types. download archive in "https://www.typescriptlang.org/cheatsheets"
 *  For uppercase, lowercase, capitalize, see "https://www.typescriptlang.org/docs/handbook/2/template-literal-types.html"
 */
export default class Test {
    /* Créer une variable "name" avec un type string qui est assigné la valeur "Saleck" */
    //public name: any;
    public name: string = "Saleck";

    /* Créer une variable "figure" avec un type number avec la valeur 333 */
    //public figure: any;
    public figure: number = 333;

    /* Créer une variable "Person" avec un objet javascript une clé "name" et "age" avec comme valeur "Saleck" et 29 */
    //public Person: any;
    public Person: {name:string,age:number} ={
        name:"Saleck",
        age:29
    };

    /* Créer une variable "numberSequence" avec un array de nombre avec le type array<number> ou number[] avec les chiffres 1, 2 et 3 */
    //public numberSequence: any;
    public numberSequence: Array<number>=[1,2,3];

    constructor() {
    }

    /*
     * Créer une méthode "uppercase" qui prends pour paramètre "str" avec le type "string".
     * avec valeur de retour "Uppercase".
     */
    //public uppercase: any;
    public uppercase = (str:string): Uppercase<string> => {
        return str.toUpperCase();
    }

    /*
     * Créer une méthode "lowercase" qui prends pour paramètre "str" avec le type "string"
     * avec valeur de retour "LOWERCASE", ensuite appeler la méthode uppercase avec en argument "name".
     */
    //public lowercase: any;
    public lowercase = (str:string): Lowercase<string> => {
        return str.toLowerCase()
    }



    /*
     * Créer une méthode "capitalize" qui prends pour paramètre "str" avec le type "string"
     * avec valeur de retour "CAPITALIZE", ensuite appeler la méthode uppercase avec en argument "name".
     */
    //public capitalize: any;
    public capitalize = (str:string): Capitalize<string> =>{
        return this.uppercase(str.charAt(0))+this.lowercase(str.slice(1))
    }

    /*
     * Créer une méthode "numberToString" pour faire passer un argument en type "number" en retour un string d'un nombre
     */
    //public numberToString: any;
    public numberToString = (num:number): string =>{
        return num.toString(10);
    }


    /*
     * Créer une méthode "stringToNumber" pour faire passer un nombre dans un string en nombre.
     */
    //public stringToNumber: any;
    public stringToNumber = (str:String): number => {
        //return +str
        return Number(str);
    }


    /*
     * Créer une méthode "operationMultiply" pour multiplier un premier nombre et un second.
     */
    //public operationMultiply: any;
    public operationMultiply = (a:number,b:number):number=>{
        return a*b;
    }

    /*
     * Créer une méthode "operationSubtraction" pour soustraire un premier nombre et un second.
     */
    //public operationSubtraction: any;
    public operationSubtraction = (a:number,b:number): number =>{
        return a-b;
    }

    /*
     * Créer une méthode "joinNumber" pour joindre avec une virgule tous les nombres d'une chaîne séparer par des espaces.
     * exemple: "1 2 4 5 6 7 8"
     */
    //public joinNumber: any;
    public joinNumber = (str:string): string => {
        // str.split(" ").join(',')
        return str.replace(/\s/g,',')
    }

    /*
     * Créer une méthode "filterById" pour filtrer un array d'object avec un id specific.
     */
    //public filterById: any;
    public filterById = (idobj: Array<Partial<{id: number}>>,idd:number): Array<Partial<{id: number}>> => {
        return idobj.filter( (x: Partial<{id: number}>) => x.id === idd);
    }

    /*
     *  Créer une méthode "removeFromArray" pour enlever un objet specifique d'un array object.
     */
    //public removeFromArray: any;

    public removeFromArray=(idobj: Array<Partial<{id:number}>>, idd:number):Array<Partial<{id:number}>> => {
        return idobj.filter(x => x.id !== idd);
    }
}
