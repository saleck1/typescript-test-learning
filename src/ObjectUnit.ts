/**
 * Object unit
 */

type OptionalProperties = {
    [key: string]: string | undefined
}

/*
 *  Create class Contact with properties
 *  1 - use export keyword
 *  2 - use class keyword
 *  3 - name this class Contact
 *  4 - In parameter of constructor add public properties:
 *      email (string)
 *      phone with Array of properties :
 *          country: number,
 *          number: number
 *      network: add properties in function of contact (optional or another) must take string for ids' record.
 */
export class Contact{
    constructor(public email:string,
                public phone:Array<{
                    country:number,
                    number:number
                }>,
                public network?: OptionalProperties) {
    }
}
/*
 *  Create below an object with class form
 *  1 - use export keyword
 *  2 - use class keyword
 *  3 - name this class AddressBook
 *  4 - In parameter of constructor add public properties:
 *      id (number),
 *      names with properties:
 *          first (string)
 *          last (string)
 *      date: add properties in function (optional or another)
 *            e.g.: birthday (Date),
 *                  carLicense: { delivery: Date, expire: Date },
 *                  idCard: { deliver: Date, expire: Date }
 *      address: { street: string, number: number, zip: number, city: string, country: string } or must be an array another Object must be a class same properties
 *      contact takes Contact Class
 *      register must be a date
 *      isActive must be a boolean
 */
export class AddressBook{
    constructor(
        public id: number,
        public names: {
            first: string,
            last: string},
        public address: {
            street: string,
            number: number,
            zip: number,
            city: string,
            country: string
        },
        public contact: Contact,
        public register: Date,
        public isActive: boolean,
        public dates?: {
            birthday: Date,
            carLicense: {
                delivery: Date,
                expire: Date
            },
            idCard:{
                delivery: Date,
                expire: Date
            }
        }) {
    }
}
/**
 *  Object Unit class for testing
 */
export default class ObjectUnit {

    /*
     *  add type Array of AddressBook
     */
    public contacts: Array<AddressBook>=[]

    constructor() {
        /*
         *  Assign contacts an AddressBook object with id 1, names: { first: "Saleck", last: "El Jili" } and oother informations
         */
    }

    /*
     *  Create method it must return firstname following by lastname in function of id in parameter. In capitalize for each.
     */
    public getNames = (id:number) : string =>{
        let f: Array<AddressBook> = this.contacts.filter( x=> ( x.id === id))
        let n: string = "" + f.map( (x: AddressBook) => x.names.first+" "+x.names.last)
        let m: string = "" + n.split(' ').map( x => x.charAt(0).toUpperCase() + x.slice(1).toLowerCase())
        return m.replace(/,/g,' ')

    }

    /*
     * Create method it must return initial of firstname and lastName in capitalize.
     */
    public getSimplifyNames = (id: number) : string =>{
        let f: Array<AddressBook> = this.contacts.filter( x=> ( x.id === id))
        let n: string=""+f.map(x=>x.names.first.charAt(0)+". "+x.names.last)
        let  m: string=""+n.split(' ').map(x=>x.charAt(0).toUpperCase()+x.slice(1).toLowerCase())
        return m.replace(/,/g,' ')
    }

    /*
     *  Create method it must return birthday date in function of id in parameter.
     */
    public getBirthday = (id: number): string =>{
        let f: Array<AddressBook> = this.contacts.filter(x=>x.id ===id)
        let m: string=""+f.map(x=>x.dates?.birthday.toISOString())
        return m
    }

    /*
     *  Create method, it must return Birthday's year of id in parameter.
     */
    public getYearBirthday = (id: number): string =>{
        let f: Array<AddressBook> = this.contacts.filter(x=>x.id===id)
        let m: string=""+f.map(x=>x.dates?.birthday.getFullYear())
        return m
    }

    /*
     *  Create method, it must return actual age in function of id in parameter.
     */
    public getAge = (id: number): number =>{
        let f: Array<AddressBook> = this.contacts.filter(x=>x.id===id)
        let m: number =+f.map(x=>Number(new Date().getFullYear())-Number(x.dates?.birthday.getFullYear()))
        return m
    }

    /*
     *  Create method, it must return address in function id and country.
     */
    public getAddress = (id: number): string =>{
        let f: Array<AddressBook> = this.contacts.filter(x=>x.id===id)
        let m:string = ""+f.map(x=>x.address.number+", "+x.address.street+"\n"+x.address.zip+" "+x.address.city)
        return m
    }

    /*
     *  Create method, it must return network profile in function of id and one platform network (Facebook).
     */
    public getNetwork = (id: number, net: string): string =>{
        let f: Array<AddressBook> = this.contacts.filter(x=>x.id===id)
        let m: string = "";


        switch (net) {
            case "linkedin" :{ m="https://www.linkedin.com/in/"
                break;}
            case "facebook" :{ m= "https://www.facebook.com/"
                break}
            case "twitter" :{ m= "https://www.twitter.com/"
                break}
            default :{ console.log("error");
                break;}
        }

        f?.map( (x: AddressBook) => {
                if (x?.contact?.network && x.contact?.network[net]) {
                    const NETWORK_ID = x?.contact?.network[net]
                    m = m + NETWORK_ID
                }
            }
        )

        return m;
    }

    /*
     *  Create method, it must return all contacts which have active.
     */
    public getAllContactsIsActive = () : {  } => {
        let f: Array<AddressBook> = this.contacts.filter(x=>x?.isActive===true)
        return f.map(x=>x)
    }

    /*
     *  Create method, it must return all contacts born in 1998 and registering in 2020 for example.
     */
    public getAllContactsBornAndRegister = (b:number, r:number) : {  } =>{
        let f: Array<AddressBook> = this.contacts.filter( x => ( Number( x.dates?.birthday.getFullYear() ) === b && Number( x.register.getFullYear() ) === r))
        return f.map(x=>x)
    }

    /*
     *  Create method with all contacts with email which domain name is orange
     */
    public getAllContactsWithEmailProvider =  (pro:string) : {} => {
        /*let pro2:string[]=pro.split('@')
        let pro3: string[]=pro2[1].split('.')*/
        let f: Array<AddressBook> = this.contacts.filter(x => x.contact.email.split('@')[1].split('.')[0] === pro )
        return f.map(x=>x)
    }
}
