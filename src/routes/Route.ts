/*
 Follow this steps:
 1 - Create class route
 2 - create properties route with type Express.Router and assign Express.router()
 3 - public path function()
 4 - At end of file, export by default an instance of Route and your property "route"
 5 - create route get which returns https://jsonplaceholder.typicode.com/users
*/
import * as express from "express";
import axios, {Axios, AxiosError, AxiosResponse} from "axios";
import {Request, Response, Router} from "express";
import UserController from "../controllers/UserController";
import Model from "../Models/Model";
import PostController from "../controllers/PostController";
import PostModel from "../Models/PostModel";


export class Route{

    public route: Router = Router({
        caseSensitive: true
    });

    constructor() {
        this.path();
    }

    private path(){
        this.getAllUsers();
        this.getUser();
        /*this.getAllPosts();
        this.getOnePost();*/
    }

    /**
     * Get all users form jsonplaceholder
     * @function
     * @private
     */
    private getAllUsers(): void {
        this.route.get("/users", async (_, res: Response) => {
            try {
                const DATA: Promise<Model> = new UserController().getAllUser()
                res.status(200).setHeader("Content-Type", "application/json; charset=\"utf-8\"").send(await DATA);
            } catch (e: any) {
                const ERROR = JSON.parse(e.message);
                res.status(ERROR?.status).json(ERROR?.message);
            }
        })
    };

    private getUser = ()=>{
        this.route.get("/users/:userId",async(req: Request, res: Response) => {
            try{
                const {userId} = req?.params;
                const data: Promise<Model> = new UserController().getOneUser(userId);
                console.log("DATA", await data)
                res.status(200).send(await data)
            } catch (e: any) {
                const ERROR = JSON.parse(e.message);
                res.status(ERROR?.status).json(ERROR?.message);
            }

        })
    };
/*
    private getAllPosts(){
        this.route.get('/posts',async (req:Request,res: Response)=>{
            try {
                const data: Promise<PostModel> = new PostController().getAllPosts();
                res.status(200).send(await data)
            } catch (e: any) {
                const ERROR = JSON.parse(e.message);
                res.status(ERROR?.status).json(ERROR?.message);
            }
        })
    };

    private getOnePost() {
        this.route.get('/posts/:id',async (req:Request,res: Response)=>{
            try {
                const {id}= req.params
                const data: Promise<PostModel> = new PostController().getOnePost(id);
                res.status(200).send(await data)
            } catch (e: any) {
                const ERROR = JSON.parse(e.message);
                res.status(ERROR?.status).json(ERROR?.message);
            }
        })
    };*/
}

export default new Route().route;
