import * as express from 'express'
import {Request, Response} from "express";
import PostModel from "../Models/PostModel";
import PostController from "../controllers/PostController";
export class PostRoute{
    /*public route= Express.Route({
        caseSensitive: true
    })*/
    public route= express.Router({
        caseSensitive: true
    })

    constructor() {
        this.getAllPosts()
        this.getOnePost()
    }



    private getAllPosts =(): void =>{
        this.route.get('/posts',async (req:Request,res: Response)=>{
            try {
                const data: Promise<PostModel> = new PostController().getAllPosts();
                res.status(200).send(await data)
            } catch (e: any) {
                const ERROR = JSON.parse(e.message);
                res.status(ERROR?.status).json(ERROR?.message);
            }
        })
    };

    private getOnePost=(): void =>{
        this.route.get('/posts/:id',async (req:Request,res: Response)=>{
            try {
                const {id}= req.params
                const data: Promise<PostModel> = new PostController().getOnePost(id);
                res.status(200).send(await data)
            } catch (e: any) {
                const ERROR = JSON.parse(e.message);
                res.status(ERROR?.status).json(ERROR?.message);
            }
        })
    };
}

export default new PostRoute().route
