import * as Net from "net";

export class DateUnit {

    constructor() {
        //console.log(this.elapsedTime(new Date("1983-10-22"), new Date("1993-01-18")))
        console.time("date");
        this.getDate(new Date("2022-02-16"));
        console.timeEnd("date");
    }

    public transformString = (date: Date): string =>{
        return date.toString()
    }

    public getDate = (date: Date): string =>{
        const FULLDATE= date.toString().split(' ')
        const RESULT: string= ""+FULLDATE.slice(0,4)
        return RESULT.replace(/,/g,' ')
    }

    public getTime = (date: Date): string =>{
        const FULLDATE= date.toString().split(' ')
        const RESULT: string= ""+FULLDATE.slice(4)
        return RESULT.replace(/,/g,' ')
    }

    public getTimezone= (date: Date): number =>{
        console.log(date.getTimezoneOffset())
        return date.getTimezoneOffset()
    }


    public getLocalDateWithEra = (date: Date,option: Partial<{lang: string,era: string,month: string,day: string, year: string}>): string =>{
        return date.toLocaleDateString(`${option.lang}`,Object(option))
    }

    public getLocalTime = (date: Date): string =>{
        return date.toLocaleTimeString()
    }

    public getUnixTime = (date: Date): number =>{
        return date.getTime()
    }

    public setHours = (date: Date,inc: boolean,heur: number): Date =>{
        date.setHours(inc?1-heur:1+heur)
        return date

    }

    public setTimes: any

    public setDate = (date:Date,newDate:{day: number,month: number, year: number}): Date =>{
        date.setFullYear(newDate.year,newDate.month-1,newDate.day)
        return date
    }

    public elapsedTime =(date1: Date,date2: Date): string =>{
        /*let months;
        months = (date2.getFullYear() - date1.getFullYear()) * 12;
        months -= date1.getMonth();
        months += date2.getMonth();
        return ""+(months <= 0 ? 0 : months)*/
        return " "

     }
}
