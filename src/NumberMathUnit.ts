export class NumberMathUnit {

    constructor() {}

    /*
     * Convert binary number to number
     */
    public binToNumber = (bin: string): number =>{
        return parseInt(bin,2)
    }
    /*
     * Convert hexadecimal to number
     */
    public hexToNumber = (hex: string): number =>{
        return parseInt(hex,16)
    }
    /*
     * Convert number to binary number
     */
    public numberToBin = (num: string,bit:number): string =>{
        let test: string= Number(num).toString(2)
        while(test.length<bit){
            test="0"+test;
        }
        return test
    }
    /*
     * Convert number to haxadecimal
     */
    public numberToHex = (num: string): string =>{
        return Number(num).toString(16)

    }
    /*
     *  Find maximum number from array number
     */
    public findMax = (arr: Array<number>): number =>{
        return Math.max(...arr)
        /*return arr.reduce(function(a: number, b: number) {
            return Math.max(a, b);
        }, -Infinity);*/
    }
    /*
     *  Find minimum number from array number
     */
    public findMin = (arr: Array<number>): number =>{
        return Math.min(...arr)
    }
    /*
     *  Find minimum ceil number from array number
     */
    public findMinToCeil = (arr: Array<number>): number =>{
        return Math.ceil(this.findMin(arr))
    }
    /*
     *  Find minimum floor number from array number
     */
    public findMinToFloor  = (arr: Array<number>): number =>{
        return Math.floor(this.findMin(arr))
    }
    /*
     *  Find average number from array number
     */
    public findAverage = (arr: Array<number>): number =>{

        let sum: number = arr.reduce(
            (previousValue: number, currentValue: number) => previousValue + currentValue
        );
        return sum/arr.length
    }
    /*
     *  Find average number with many decimal from array number
     */
    public findAverageWithDecimal = (arr: Array<number>,n:number): number =>{
        const RESULT: string = this.findAverage(arr).toFixed(n)
        return +RESULT
    }
    /*
     *  Find near to zero number from array number
     */
    public nearToZero = (arr: Array<number>): number => {

        const ARR_ABSOLUT: Array<number> = arr.map(x=>Math.abs(x))

        const INDEX: number= ARR_ABSOLUT.indexOf(this.findMin(ARR_ABSOLUT))

        return arr[INDEX]

        /*let goal: number = 0;
        let closest = arr.reduce(function(prev: number, curr: number) {
            return (Math.abs(curr - goal) < Math.abs(prev - goal) ? curr : prev);
        });

        return closest*/
    }
    /*
     *  Find far to zero number from array number
     */
    public farToZero  = (arr: Array<number>): number => {

        const ARR_ABSOLUT: Array<number> = arr.map(x=>Math.abs(x))

        const INDEX: number= ARR_ABSOLUT.indexOf(this.findMax(ARR_ABSOLUT))

        return arr[INDEX]


        /*return arr.reduce((prev:number, curr:number) =>
            ((Math.abs(curr) > Math.abs(prev) ? curr : prev))
        );*/
    }
}

